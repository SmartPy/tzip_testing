class Failed: pass
failed = Failed()

class Spec:
    def __repr__(self):
        return "%s(%s)" % (self.prim,
                    ", ".join(str(v) for v in self.args))

class Value:
    def __repr__(self):
        return str(self.value)

def prim(prim, *args):
    spec = Spec()
    spec.prim = prim
    spec.args = args
    return spec

def let_in(name, from_value, in_value):
    return prim("let_in", name, from_value, in_value)

def not_(arg):
    return prim("not", arg)

def and_(*args):
    return prim("and", *args)

def or_(*args):
    return prim("or", *args)

def exists(value):
    return prim("exists", value)

def get_env(var):
    return prim("get_env", var)

def get_entrypoint(c, ep):
    return prim("get_entrypoint", c, ep)

def get_entrypoint_tparameter(ep):
    return prim("get_entrypoint_tparameter", ep)

def cst(value):
    if isinstance(value, bool):
        result = Value()
        result.prim = "cst"
        result.value = "true" if value else "false"
        result.t = "bool"
        return result
    return None

def cst_contract(entrypoints = []):
    result = Value()
    result.prim = "cst"
    result.value = {}
    result.value["entrypoints"] = entrypoints
    return result

def compute(expr, env):
    def trace(s):
        if True:
            print(s)
    def trace_in(expr):
        trace("in   " + str(expr))
    def trace_out(expr):
        trace("out  " + str(expr))
    def trace_failed(expr, loc = None):
        trace("FAIL " + str(expr) + (" " + loc if loc else ""))
    trace_in(expr)
    if expr.prim == "let_in":
        name, from_value, in_value = expr.args
        from_value = compute(from_value, env)
        if from_value == failed:
            trace_failed(expr)
            return failed
        env2 = env.copy()
        env2[name] = from_value
        res = compute(in_value, env2)
        trace_out(expr)
        return res
    if expr.prim == "cst":
        trace_out(expr)
        return expr
    if expr.prim == "get_entrypoint_tparameter":
        ep, = expr.args
        ep = compute(ep, env)
        if ep == failed:
            trace_failed(expr, "ep")
            return failed
        trace_out(expr)
        return ep[1]
    if expr.prim == "get_entrypoint":
        c, ep = expr.args
        c = compute(c, env)
        if c == failed:
            trace_failed(expr, "c")
            return failed
        eps = c.value["entrypoints"]
        try:
            res = eps[ep]
            trace_out(expr)
            return res
        except KeyError:
            trace_failed(expr, "ep")
            return failed
    if expr.prim == "get_env":
        name, = expr.args
        try:
            res = env[name]
            trace_out(expr)
            return res
        except KeyError:
            trace_failed(expr)
            return failed
    if expr.prim == "not":
        arg, = expr.args
        x = compute(arg, env)
        if x == failed:
            trace_failed(expr)
            return failed
        if x.value == "false":
            trace_out(expr)
            return cst(True)
        trace_out(expr)
        return cst(False)
    if expr.prim == "and":
        for arg in expr.args:
            x = compute(arg, env)
            if x == failed:
                trace_failed(expr)
                return failed
            if x.value == "false":
                trace_out(expr)
                return cst(False)
        trace_out(expr)
        return cst(True)
    if expr.prim == "or":
        for arg in expr.args:
            x = compute(arg, env)
            if x == failed:
                trace_failed(expr)
                return failed
            if x.value == "true":
                trace_out(expr)
                return cst(True)
        trace_out(expr)
        return cst(False)
    if expr.prim == "exists":
        arg, = expr.args
        x = compute(arg, env)
        if x == failed:
            trace_out(expr)
            return cst(False)
        return cst(True)
    raise Exception("unknown expr: " + str(expr))

example_1 = let_in("ep", get_entrypoint(get_env("c"), "transfer"), get_entrypoint_tparameter(get_env("ep")))
example_2 = let_in("ep", get_entrypoint(get_env("c"), "getBalance"), get_entrypoint_tparameter(get_env("ep")))
example_3 = let_in("ep", get_entrypoint(get_env("c"), "random_entry_point"), cst(True))

env1 = {"c" : cst_contract(entrypoints = {"transfer" : ("int", "int list"), "getBalance": ("string", "nat")})}

print("===")

print(compute(example_1, env1))

print("===")

print(compute(example_2, env1))

print("===")

print(compute(and_(*[exists(x) for x in [example_1, example_2]]), env1))

print("===")

print(compute(not_(exists(example_3)), env1))

print("===")

"""
Expressions:
  prim(arg0, ..., argn) where prim is a string and argi are expressions


Constants:

string: length(1), concat(2)
int: add(2), sub(2), minus(1), mul(2), div(2)
bool: and(n), or(n)
  and() = true
  or() = false
  or(false*, true, ...) = true,
  or(false*, failed, ...) = failed
dict:

"""
