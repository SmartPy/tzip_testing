type type_cst = string (* todo *)

type entrypoint =
  { name : string
  ; tparameter : type_cst
  ; tstorage : type_cst }

let entrypoint name tparameter tstorage = {name; tparameter; tstorage}

let string_of_entrypoint {name; tparameter; tstorage} =
  Printf.sprintf "(%s %S %S)" name tparameter tstorage

type cst =
  | Failed
  | String     of string
  | Int        of int
  | Bool       of bool
  | Contract   of {entrypoints : entrypoint list}
  | Entrypoint of entrypoint

let string_of_cst = function
  | Failed -> "failed"
  | String x -> Printf.sprintf "%S" x
  | Int i -> string_of_int i
  | Bool b -> string_of_bool b
  | Contract {entrypoints} ->
      Printf.sprintf
        "(contract (entrypoints %s))"
        (String.concat " " (List.map string_of_entrypoint entrypoints))
  | Entrypoint ep -> string_of_entrypoint ep

type expr =
  | Prim of
      { name : string
      ; args : expr list }
  | Cst  of cst

let string s = Cst (String s)

let bool s = Cst (Bool s)

let rec string_of_expr = function
  | Prim {name; args} ->
      Printf.sprintf
        "(%s %s)"
        name
        (String.concat " " (List.map string_of_expr args))
  | Cst cst -> string_of_cst cst

let prim name args = Prim {name; args}

let let_in name from_value in_value =
  prim "let_in" [Cst (String name); from_value; in_value]

let not_ arg = prim "not" [arg]

let and_ args = prim "and" args

let or_ args = prim "or" args

let exists value = prim "exists" [value]

let get_env var = prim "get_env" [Cst (String var)]

let get_entrypoint c ep = prim "get_entrypoint" [c; ep]

let get_entrypoint_tparameter ep = prim "get_entrypoint_tparameter" [ep]

let trace_in expr =
  if true then Printf.ksprintf print_endline "in   %s " (string_of_expr expr)

let trace_out expr =
  if true then Printf.ksprintf print_endline "out  %s " (string_of_expr expr)

let trace_failed expr =
  if true then Printf.ksprintf print_endline "FAIL %s " (string_of_expr expr)

let ( >>= ) x f =
  match x with
  | Failed -> Failed
  | x -> f x

let ( let* ) = ( >>= )

let rec compute env expr =
  trace_in expr;
  let on_bool f x =
    match x with
    | Bool x -> Bool (f x)
    | _ -> Failed
  in
  let res =
    match expr with
    | Cst cst ->
        trace_out expr;
        cst
    | Prim {name = "not"; args = [x]} -> compute env x |> on_bool not
    | Prim {name = "and"; args} ->
        List.fold_left
          (fun res x ->
            match res with
            | Bool false -> Bool false
            | Bool true -> compute env x
            | _ -> Failed)
          (Bool true)
          args
    | Prim {name = "or"; args} ->
        List.fold_left
          (fun res x ->
            match res with
            | Bool true -> Bool true
            | Bool false -> compute env x
            | _ -> Failed)
          (Bool true)
          args
    | Prim {name = "let_in"; args = [Cst (String var); from_value; in_value]} ->
        let* v = compute env from_value in
        let env2 = (var, v) :: List.remove_assoc var env in
        compute env2 in_value
    | Prim {name = "get_entrypoint"; args = [c; Cst (String ep)]} ->
        let* c = compute env c in
        begin
          match c with
          | Contract {entrypoints} ->
            ( match List.find_opt (fun {name} -> name = ep) entrypoints with
            | None -> Failed
            | Some ep -> Entrypoint ep )
          | _ -> Failed
        end
    | Prim {name = "get_entrypoint_tparameter"; args = [ep]} ->
        let* ep = compute env ep in
        begin
          match ep with
          | Entrypoint {tparameter} -> String tparameter
          | _ -> Failed
        end
    | Prim {name = "get_env"; args = [Cst (String var)]} ->
      begin
        match List.assoc_opt var env with
        | None -> Failed
        | Some v -> v
      end
    | Prim {name = "exists"; args = [e]} ->
      begin
        match compute env e with
        | Failed -> Bool false
        | _ -> Bool true
      end
    | Prim {name} ->
        Printf.ksprintf failwith "Unknown primitive or number of args %s" name
  in
  begin
    match res with
    | Failed -> trace_failed expr
    | _ -> trace_out expr
  end;
  res

let example_1 =
  let_in
    "ep"
    (get_entrypoint (get_env "c") (string "transfer"))
    (get_entrypoint_tparameter (get_env "ep"))

let example_2 =
  let_in
    "ep"
    (get_entrypoint (get_env "c") (string "getBalance"))
    (get_entrypoint_tparameter (get_env "ep"))

let example_3 =
  let_in
    "ep"
    (get_entrypoint (get_env "c") (string "random_entry_point"))
    (bool true)

let env1 =
  [ ( "c"
    , Contract
        { entrypoints =
            [ entrypoint "transfer" "int" "int list"
            ; entrypoint "getBalance" "string" "nat" ] } ) ]

let pp cst = print_endline (string_of_cst cst)

let test env example =
  print_endline "======";
  Printf.ksprintf print_endline "Testing %s" (string_of_expr example);
  let res = compute env example in
  Printf.ksprintf print_endline "Result: %s" (string_of_cst res)

let () = test env1 example_1

let () = test env1 example_2

let () = test env1 example_3

let () = test env1 (exists example_3)

let () = test env1 (get_env "c")

let () = test env1 (and_ (List.map exists [example_1; example_2; example_3]))

let () = test env1 (and_ (List.map exists [example_1; example_2]))

let () = test env1 (or_ (List.map exists [example_1; example_2; example_3]))

let () = test env1 (or_ (List.map exists [example_1; example_2]))
